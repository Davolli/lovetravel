# LoveTravel

Documento da API V1 do sistema de vendas de pacotes turisticos.

**Base**  [ https://turismo-e3aa8.firebaseio.com/ ]

## Categorias
----

### **GET /v1/categorias.json**

Recebe as categorias.

##### Acesso
* Publico.

##### Parâmetros
* / - Recebe todas categorias
* /< slug-categoria >/ - Recebe dados de uma categoria específica

##### Code
* 200 - Ok.

##### Exemplo

```
(GET) https://turismo-e3aa8.firebaseio.com/v1/categorias.json

200 - Ok.
{
  "arvorismo": {
    "nome": "Arvorismo",
    "slug": "arvorismo"
  },
  "aventura": {
    "nome": "Aventura",
    "slug": "aventura"
  },
  ....
}

(GET) https://turismo-e3aa8.firebaseio.com/v1/categorias/arvorismo.json

200 - Ok.
{
  "nome": "Arvorismo",
  "slug": "arvorismo"
}

(GET) https://turismo-e3aa8.firebaseio.com/v1/categorias/nomeNaoExiste.json

200 - Ok.

null

```

----
### **PUT /v1/categorias.json** 

Cria ou atualiza categorias

##### Acesso
* Privado.

##### Parâmetros
* Nenhum.

##### Code
* 200 - Ok.
* 401 - Unauthorized. 

##### Exemplo


```
(PUT) https://turismo-e3aa8.firebaseio.com/v1/categorias/nome-categoria.json
* Nome: Nome Categoria
* Slug: nome-categoria

200 - ok
{
  "nome": "Nome categoria",
  "slug": "nome-categoria"
}

401 - Unauthorized
{
  "error": "Permission denied"
}
```