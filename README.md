# LoveTravel

Documento da API V1 do sistema de vendas de pacotes turisticos.

**Base**  [ https://turismo-e3aa8.firebaseio.com/ ]

```
  var config = {
    apiKey: "AIzaSyBnMgHGiUyExWaklX-9fCzTYlWCZDzTPVs",
    authDomain: "turismo-e3aa8.firebaseapp.com",
    databaseURL: "https://turismo-e3aa8.firebaseio.com",
    projectId: "turismo-e3aa8",
    storageBucket: "turismo-e3aa8.appspot.com",
    messagingSenderId: "119552639569"
  };
```

## Categorias
----

### **GET /v1/categorias.json**

Recebe as categorias.

##### Acesso
* Publico.

##### Parâmetros
* / - Recebe todas categorias
* /< slug-categoria >/ - Recebe dados de uma categoria específica

##### Code
* 200 - Ok.

##### Exemplo

```
(GET) https://turismo-e3aa8.firebaseio.com/v1/categorias.json

200 - Ok.
{
  "arvorismo": {
    "nome": "Arvorismo",
    "slug": "arvorismo"
  },
  "aventura": {
    "nome": "Aventura",
    "slug": "aventura"
  },
  ....
}

(GET) https://turismo-e3aa8.firebaseio.com/v1/categorias/arvorismo.json

200 - Ok.
{
  "nome": "Arvorismo",
  "slug": "arvorismo"
}

(GET) https://turismo-e3aa8.firebaseio.com/v1/categorias/nomeNaoExiste.json

200 - Ok.

null

```

----
### **PUT /v1/categorias.json** 

Cria ou atualiza categorias

##### Acesso
* Privado.

##### Parâmetros
* Nenhum.

##### Code
* 200 - Ok.
* 401 - Unauthorized. 

##### Exemplo


```
(PUT) https://turismo-e3aa8.firebaseio.com/v1/categorias/nome-categoria.json
* Nome: Nome Categoria
* Slug: nome-categoria

200 - ok
{
  "nome": "Nome categoria",
  "slug": "nome-categoria"
}

401 - Unauthorized
{
  "error": "Permission denied"
}
```

## Passeios
----

### **GET /v1/passeios.json**

Recebe os passeios.

##### Acesso
* Publico.

##### Parâmetros
* / - Recebe todas passeios por categoria
* /< slug-categoria >/ - Recebe dados dos passeios de uma categoria específica

##### Code
* 200 - Ok.

##### Exemplo

```
(GET) https://turismo-e3aa8.firebaseio.com/v1/passeios.json

200 - Ok.
{
  "arvorismo": {
    {
      "passeio-de-barco-na-lagoa": {
        "animais": "n",
        "cadeirantes": "n",
        "contatos": {
          "348c3996f452": "(48) 9 8811-7852",
          "4c8869400060": "(22) 9 8811-8899"
        },
        "criancas": "s",
        "descricao": "Lorem ipsum dolor sit amet, consecteturo eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "endereco": "Rua das flores, 42, Paraty - RJ",
        "fotos": {
          "2bd8ba36e425": "/assets/img/portfolio/single-project-1.jpg",
          "52fddab00dbf": "/assets/img/portfolio/single-project-3.jpg",
          "f6ea9baa9813": "/assets/img/portfolio/single-project-2.jpg"
        },
        "nome": "Passeio de barco na lagoa",
        "recomendacoes": "Levar toalha e roupas de banho",
        "slug": "passeio-de-barco-na-lagoa"
      }
    }
  },
  "aventura": {
    {
      "passeio-de-bicicleta-na-lagoa": {
        "animais": "n",
        "cadeirantes": "n",
        "contatos": {
          "348c3996f452": "(48) 9 8811-7852",
          "4c8869400060": "(22) 9 8811-8899"
        },
        "criancas": "s",
        "descricao": "Lorem ipsum dolor sit amet, consecteturo eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "endereco": "Rua das flores, 42, Paraty - RJ",
        "fotos": {
          "2bd8ba36e425": ""https://firebasestorage.googleapis.com/v0/b/turismo-e3aa8.appspot.com/o/fotos%2Fsingle-project-1.jpg?alt=media&token=25002357-9161-46b5-9112-de88579b79fa"",
          "52fddab00dbf": ""https://firebasestorage.googleapis.com/v0/b/turismo-e3aa8.appspot.com/o/fotos%2Fsingle-project-2.jpg?alt=media&token=df687dd5-fa61-4c0a-be6e-56819daea396"",
          "f6ea9baa9813": ""https://firebasestorage.googleapis.com/v0/b/turismo-e3aa8.appspot.com/o/fotos%2Fsingle-project-3.jpg?alt=media&token=b038e0a4-5d26-46f1-b500-35f00a628cbc""
        },
        "nome": "Passeio de bicicleta na lagoa",
        "recomendacoes": "Levar toalha e roupas de banho",
        "slug": "passeio-de-bicicleta-na-lagoa"
      }
    }
  }
}

(GET) https://turismo-e3aa8.firebaseio.com/v1/passeios/arvorismo.json

200 - Ok.
{
  "passeio-de-barco-na-lagoa": {
    "animais": "n",
    "cadeirantes": "n",
    "contatos": {
      "348c3996f452": "(48) 9 8811-7852",
      "4c8869400060": "(22) 9 8811-8899"
    },
    "criancas": "s",
    "descricao": "Lorem ipsum dolor sit amet, consecteturo eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    "endereco": "Rua das flores, 42, Paraty - RJ",
    "fotos": {
      "2bd8ba36e425": "https://firebasestorage.googleapis.com/v0/b/turismo-e3aa8.appspot.com/o/fotos%2Fsingle-project-1.jpg?alt=media&token=25002357-9161-46b5-9112-de88579b79fa",
      "52fddab00dbf": "https://firebasestorage.googleapis.com/v0/b/turismo-e3aa8.appspot.com/o/fotos%2Fsingle-project-2.jpg?alt=media&token=df687dd5-fa61-4c0a-be6e-56819daea396",
      "f6ea9baa9813": "https://firebasestorage.googleapis.com/v0/b/turismo-e3aa8.appspot.com/o/fotos%2Fsingle-project-3.jpg?alt=media&token=b038e0a4-5d26-46f1-b500-35f00a628cbc"
    },
    "nome": "Passeio de barco na lagoa",
    "recomendacoes": "Levar toalha e roupas de banho",
    "slug": "passeio-de-barco-na-lagoa"
  }
}

(GET) https://turismo-e3aa8.firebaseio.com/v1/passeios/nomeNaoExiste.json

200 - Ok.

null

```
### **PUT /v1/passeios.json** 

Cria ou atualiza categorias

##### Acesso
* Privado.

##### Parâmetros
* /< nome-categoria >/ - Cadastra/atualiza um passeio
    * nome - nome do passeio
    * slug - slug do passeio

##### Code
* 200 - Ok.
* 401 - Unauthorized. 

##### Exemplo


```
(PUT) https://turismo-e3aa8.firebaseio.com/v1/passeios/categoria/nome-passeio.json
* Nome: Nome passeio
* Slug: nome-passeio

200 - ok
{
  "nome": "Nome Passeio",
  "slug": "nome-passeio"
}

401 - Unauthorized
{
  "error": "Permission denied"
}
```
## Contato
----
### **POST /v1/contato.json** 

Envia uma mensagem de contato para administrador do site

##### Acesso
* Livre para escrita.

##### Parâmetros
* Nome: Nome usuario
* Email: email@contato.com
* Mensagem: lorem ipsum

##### Code
* 200 - Ok.
* 401 - Unauthorized. 

##### Exemplo


```
(POST) https://turismo-e3aa8.firebaseio.com/v1/contato.json
* Nome: Nome usuario
* Email: email@contato.com
* Mensagem: lorem ipsum

200 - ok
{
  "name": "-KwCZZySrRNkSifbh5aE"
}

401 - Unauthorized
{
  "error": "Permission denied"
}
```